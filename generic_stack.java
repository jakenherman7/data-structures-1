package telescope_receiver;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;
import static telescope_receiver.Telescope_Receiver.scan;

/**
 * @author jaken herman
 * @param <T>
 */
public class generic_stack<T> {
    
    static Scanner scan = new Scanner(System.in);
    
    private int top = 0;
    private final int EMPTY = 0;
    static int max = setMax();
    
    public static int setMax(){
       System.out.println("Input Stack Size: ");
       int x;
       
       try {
            x = scan.nextInt();
            if(x == 0){
                System.out.println("Invalid Stack Size. Size of stack has defaulted"
                    + " to 5");
                x = 5;
            }
            } catch (InputMismatchException e){
                System.out.println("Invalid Stack Size. Size of stack has defaulted"
                        + " to 5");
                x = 5;
            }
       
       return x;
    }
    
    static String[] stackArray = new String[max + 1];
    
    static int getMax(){
        return max;
    }
       
    void push(String x){
        
        if(x.contains(",")){
            String[] intArray = x.split(",");
            String temp;
            
            for (int i = 0; i < (intArray.length / 2); i++){
                temp = intArray[i];
                intArray[i] = intArray[intArray.length - 1 - i];
                intArray[intArray.length - 1 - i] = temp;
            }
            
            String intArrayString = Arrays.toString(intArray);
            int k = 0; //counter for occurences of ','
            for (int j = 0; j < intArrayString.length(); j++){
                if (intArrayString.charAt(j) == ','){
                    k += 1;
                }
            }
            
            if(top == max){
                System.out.println("Error: Stack Overflow.");
            }
            else {
                stackArray[++top] = k + ", " + intArrayString.substring(5, intArrayString.length() - 1);
            }
        }
            
        if(top == max){
            System.out.println("Error: Stack Overflow.");
        }
        
        if (!x.contains(",")) {
            String k = (String)x;
            String y = new StringBuilder(k).reverse().toString();
            stackArray[++top] = y;
            stackArray[top] = "" + Integer.toString(k.length()) + y;
        }
    }
    
    void pop(){
        if (top != EMPTY){
            System.out.println(stackArray[top] + " has been popped from the stack"); 
            top -= 1;
        }
        else {
            System.out.println("Underflow.");
        }
    }
    
    boolean empty(){
        return top == EMPTY;
    }
    
    boolean full(){
        return top == (max - 1);
    }
}

package telescope_receiver;
import java.util.*;
/**
*
* @author jaken herman
*/
public class Telescope_Receiver extends generic_stack {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        String stackType;
        Integer max = null; //size of stack
        System.out.println("Input Stack Type: ");
        stackType = scan.nextLine();
        if (stackType.equalsIgnoreCase("string")) {
            generic_stack < String > stringStack = new generic_stack();
            stringStack(stringStack);
            } else if ((stackType.equalsIgnoreCase("int")) || (stackType.equalsIgnoreCase("integer"))) {
            generic_stack < Integer > intStack = new generic_stack();
            intStack(intStack);
            } else {
            System.out.println("Invalid stack type. Stack has defaulted to Integer type.");
            generic_stack < Integer > intStack = new generic_stack();
            intStack(intStack);
        }
    }
    public static void intStack(generic_stack < Integer > stack) {
        System.out.println("Enter Operations: ");
        for (int i = 0; i <= generic_stack.getMax(); i++) {
            String userString = scan.nextLine();
            try {
                if (userString.substring(0, 1).equalsIgnoreCase("Q")){
                    System.out.println("Program Complete");
                    i = generic_stack.getMax();
                }
                else if (userString.substring(0, 3).equalsIgnoreCase("Pop")) {
                    i -= 2;
                    stack.pop();
                    } else if (userString.substring(0, 6).equalsIgnoreCase("Insert")) {
                    if(i == generic_stack.getMax()){
                        System.out.println("Failed to insert '" + userString.substring(7) + "' to the stack"
                        + " because the stack is full.");
                        System.out.println("Pop or quit the program.");
                        i -= 1;
                    }
                    else {
                        stack.push(userString.substring(7));
                    }
                }
                else {
                    System.out.println("Invalid Operation. Try again");
                    i -= 1;
                }
                } catch (StringIndexOutOfBoundsException e) {
                System.out.println("Try again. If you'd like to quit the program, type Q");
                i -= 1;
            }
        }
    }
    public static void stringStack(generic_stack < String > stack) {
        System.out.println("Enter Operations: ");
        for (int i = 0; i <= generic_stack.getMax(); i++) {
            String userString = scan.nextLine();
            try {
                if (userString.substring(0, 1).equalsIgnoreCase("Q")){
                    System.out.println("Program Complete");
                    i = generic_stack.getMax();
                }
                else if (userString.substring(0, 3).equalsIgnoreCase("Pop")) {
                    i -= 2;
                    stack.pop();
                    } else if (userString.substring(0, 6).equalsIgnoreCase("Insert")) {
                    if(i == generic_stack.getMax()){
                        System.out.println("Failed to insert '" + userString.substring(7) + "' to the stack"
                        + " because the stack is full.");
                        System.out.println("Pop or quit the program.");
                        i -= 1;
                    }
                    else {
                        stack.push(userString.substring(7));
                    }
                }
                else {
                    System.out.println("Invalid Operation. Try again");
                    i -= 1;
                }
                } catch (StringIndexOutOfBoundsException e) {
                System.out.println("Try again. If you'd like to quit the program, type Q");
                i -= 1;
            }
        }
    }
} 